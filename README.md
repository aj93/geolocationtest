### Contexte



Le but est de pouvoir manipuler des positions GPS (latitude et longitude).



### Côté API



L'API devra pouvoir permettre :

* De créer une position GPS ;
* De lister les positions GPS ;
* De supprimer une position GPS ;
* De dire si une position GPS est à plus ou moins 10 km d’une autre position GPS ;


A noter que la mise à jour d’une position GPS n'est pas demandée dans le cadre de ce test.



### Remarques



Le backend DOIT ÊTRE sous Java 17 et Spring Boot 3.0.x.



Le code DOIT ÊTRE disponible sur un repository GIT (github, gitlab, bitbucket etc.)



Tu peux utiliser n’importe quelle autre librairie Java en dehors de l’écosystème Spring.



Tu as le choix de la base de données.



Pour rappel, le but pour nous est d’avoir une base de discussion pour l’entretien technique qui suivra.



Un lien pour t’aider à démarrer : https://start.spring.io/
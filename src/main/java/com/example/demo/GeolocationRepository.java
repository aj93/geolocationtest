package com.example.demo;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GeolocationRepository extends CrudRepository<Geolocation, Long> {
    Geolocation findById(long id);
    List<Geolocation> findAll();
}

package com.example.demo;

public class GeolocationException extends RuntimeException {
    public GeolocationException(String message) {
        super(message);
    }
}

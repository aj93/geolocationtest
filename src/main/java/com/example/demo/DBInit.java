package com.example.demo;

import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

@Component
public class DBInit {
    GeolocationRepository geolocationRepository;

    public DBInit(GeolocationRepository geolocationRepository) {
        this.geolocationRepository = geolocationRepository;
    }

    @PostConstruct
    public void defaultData() {
        this.geolocationRepository.save(
                // Baltimore
                new Geolocation(39.28d, 76.60d)
        );
        this.geolocationRepository.save(
                // Boston
                new Geolocation(42.36d, -71.05d)
        );
    }
}

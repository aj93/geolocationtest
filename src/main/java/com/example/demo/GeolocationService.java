package com.example.demo;


import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class GeolocationService {
    private static final int EARTH_RADIUS = 6371; // In kilometer
    private GeolocationRepository geolocationRepository;

    public GeolocationService(GeolocationRepository geolocationRepository) {
        this.geolocationRepository = geolocationRepository;
    }

    public void saveGeolocation(Double latitude, Double longitude) {
        this.geolocationRepository.save(
                new Geolocation(
                        Objects.requireNonNull(latitude),
                        Objects.requireNonNull(longitude)
                )
        );
    }

    public boolean isLessThanTenKilometer(long fromGeolocationId, long toGeolocationId) {
        // acos(sin(lat1)*sin(lat2)+cos(lat1)*cos(lat2)*cos(lon2-lon1))*6371
        // (cf., https://community.powerbi.com/t5/Desktop/How-to-calculate-lat-long-distance/td-p/1488227#:~:text=You%20need%20Latitude%20and%20Longitude,is%20Earth%20radius%20in%20km.)
        
        Geolocation x = this.geolocationRepository.findById(fromGeolocationId);
        Geolocation y = this.geolocationRepository.findById(toGeolocationId);
        
        if (x == null) {
            throw new GeolocationException(String.format("Geolocation with id %d does not exist", fromGeolocationId));
        }
        if (y == null) {
            throw new GeolocationException(String.format("Geolocation with id %d does not exist", toGeolocationId));
        }
        double res = Math.acos(
                        Math.sin(x.getLatitude()) * Math.sin(y.getLatitude()) +
                            Math.cos(x.getLatitude()) * Math.cos(y.getLatitude()) *
                                Math.cos(y.getLongitude() - x.getLongitude())) * EARTH_RADIUS;

        return res < 10.d;
    }

    public Geolocation findGeolocation(long id) {
        Geolocation geolocation = this.geolocationRepository.findById(id);
        if (geolocation == null) {
            throw new GeolocationException(String.format("Geolocation with id %d does not exist", id));
        }
        return geolocation;
    }

    public List<Geolocation> listGeolocation() {
        return this.geolocationRepository.findAll();
    }

    public void deleteGeolocation(long id) {
        this.geolocationRepository.deleteById(Objects.requireNonNull(id));
    }
}

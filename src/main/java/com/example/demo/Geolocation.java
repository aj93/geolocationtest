package com.example.demo;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;



@NoArgsConstructor
@Data
@Entity
public class Geolocation {
    @Id
    @GeneratedValue
    private long id;

    @NonNull
    private Double latitude;

    @NonNull
    private Double longitude;

    public Geolocation(@NonNull Double latitude, @NonNull Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}

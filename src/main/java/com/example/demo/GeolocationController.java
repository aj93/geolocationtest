package com.example.demo;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class GeolocationController {
    GeolocationService geolocationService;

    public GeolocationController(GeolocationService geolocationService) {
        this.geolocationService = geolocationService;
    }

    @GetMapping("/geolocation/{geolocationId}")
    public ResponseEntity<Geolocation> getGeolocation(@PathVariable long geolocationId) {
        try {
            Geolocation geolocation = this.geolocationService.findGeolocation(geolocationId);
            return new ResponseEntity<>(geolocation, HttpStatus.OK);
        } catch (GeolocationException ge) {
            return new ResponseEntity(ge.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/geolocation/distance")
    public ResponseEntity<String> isLessThanTenKilometer(@RequestParam(value = "fromGeolocationId") long fromGeolocationId, @RequestParam(value = "toGeolocationId") long toGeolocationId) {
        try {
            boolean isLessThanTenKilometer = geolocationService.isLessThanTenKilometer(fromGeolocationId, toGeolocationId);
            return isLessThanTenKilometer ? new ResponseEntity(String.format("%d is less than 10 km from %d", fromGeolocationId, toGeolocationId), HttpStatus.OK) :
                    new ResponseEntity(String.format("%d is more than 10 km from %d", fromGeolocationId, toGeolocationId), HttpStatus.OK);
        } catch (GeolocationException eg) {
            return new ResponseEntity(eg.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/geolocation/createGeolocation")
    public ResponseEntity<String> saveGeolocation(@RequestParam(value = "latitude") double latitude, @RequestParam(value = "longitude") double longitude) {
        try {
            this.geolocationService.saveGeolocation(latitude, longitude);
            return new ResponseEntity("Geolocation saved", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/geolocation/listAllGeolocations")
    public ResponseEntity<List<Geolocation>> getAllGeolocations() {
        try {
            List<Geolocation> geolocations = this.geolocationService.listGeolocation();
            return new ResponseEntity(geolocations, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/geolocation/deleteGeolocation")
    public ResponseEntity<String> deleteGeolocation(@RequestParam(value = "id") long id) {
        try {
            this.geolocationService.deleteGeolocation(id);
            return new ResponseEntity(String.format("Geolocation with id %d deleted", id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
}
